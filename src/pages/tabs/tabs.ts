import { Component } from '@angular/core';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'HomePage';
  tab2Root = 'TopratedPage';
  tab3Root = 'FollowersPage';
  tab4Root = 'FollowingPage';

  constructor() {

  }
}
