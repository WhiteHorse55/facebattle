import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {

  member_array : any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.member_array = []
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
    this.generateArray()
  }

  generateArray()
  {
    this.member_array = [
      {"gender" : "female","img" : "woman1.jpg","name":"Name1"},
      {"gender" : "female","img" : "woman2.jpg","name":"Name2"},
      {"gender" : "female","img" : "woman3.jpg","name":"Name3"},
      {"gender" : "female","img" : "woman4.jpg","name":"Name4"},
      {"gender" : "female","img" : "woman5.jpg","name":"Name5"},
    ]
  }

}
