import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { CustomcardComponent } from './customcard/customcard';
import { VotingComponent } from './voting/voting';
import { CustombarComponent } from './custombar/custombar';
@NgModule({
	declarations: [CustomcardComponent,
    VotingComponent,
    CustombarComponent],
	imports: [IonicModule],
	exports: [CustomcardComponent,
    VotingComponent,
    CustombarComponent]
})
export class ComponentsModule {}
