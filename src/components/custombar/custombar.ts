import { Component } from '@angular/core';

/**
 * Generated class for the CustombarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custombar',
  templateUrl: 'custombar.html'
})
export class CustombarComponent {

  text: string;

  constructor() {
    console.log('Hello CustombarComponent Component');
    this.text = 'Hello World';
  }

}
