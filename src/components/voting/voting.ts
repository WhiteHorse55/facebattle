import { Component, Output, EventEmitter } from '@angular/core';
import { EventListener } from '@angular/core/src/debug/debug_node';

/**
 * Generated class for the VotingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'voting',
  templateUrl: 'voting.html'
})
export class VotingComponent {

  text: string;

  @Output() clickFemale : EventEmitter<any> = new EventEmitter()

  constructor() {
    console.log('Hello VotingComponent Component');
    this.text = 'Hello World';
  }

  onclickfemalebutton()
  {

  }
  
  onclickmalebutton()
  {

  }

}
