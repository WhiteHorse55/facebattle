import { Component, Input } from '@angular/core';

/**
 * Generated class for the CustomcardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'customcard',
  templateUrl: 'customcard.html'
})
export class CustomcardComponent {

  @Input() member : any
  constructor() {
    console.log('Hello CustomcardComponent Component');
  }

}
